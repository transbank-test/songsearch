package cl.stgo.songsearch.api;

import java.io.Serializable;
import java.util.ArrayList;

import cl.stgo.songsearch.models.Song;

public class ResponseDAO implements Serializable {
    private int resultCount;
    private ArrayList<Song> results;

    public ResponseDAO() {
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public ArrayList<Song> getResults() {
        return results;
    }

    public void setResults(ArrayList<Song> results) {
        this.results = results;
    }
}
