package cl.stgo.songsearch.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    //https://itunes.apple.com/search?mediaType=music&limit=100&entity=song
    @GET("/search?mediaType=music&limit=1&entity=song")
    Call<ResponseDAO> searchSongs(@Query("term") String songName);

    //https://itunes.apple.com/lookup?id=1440858699&entity=song
    @GET("/lookup?entity=song")
    Call<ResponseDAO> searchAlbum(@Query("id") String albumId);
}



