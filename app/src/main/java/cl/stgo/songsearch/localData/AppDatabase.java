package cl.stgo.songsearch.localData;

import android.content.Context;
import android.util.Log;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import cl.stgo.songsearch.localData.dao.AlbumDao;
import cl.stgo.songsearch.localData.dao.SongDao;
import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.Song;

@Database(entities = {Song.class, Album.class}, version = 1, exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    private static final String LOG_TAG = AppDatabase.class.getSimpleName();
    private static final Object LOCK = new Object();
    private static final String DATABASE_NAME = "songSearch";
    private static AppDatabase instance;


    public abstract SongDao songDao();
    public abstract AlbumDao albumDao();

    public static AppDatabase getInstance(Context context) {
        if (instance == null) {
            synchronized (LOCK) {
                Log.d(LOG_TAG, "Creating new database");
                instance = Room.databaseBuilder(context.getApplicationContext(),
                        AppDatabase.class, AppDatabase.DATABASE_NAME)
                        .build();
            }
        }
        Log.d(LOG_TAG, "Getting the database");
        return instance;
    }

}
