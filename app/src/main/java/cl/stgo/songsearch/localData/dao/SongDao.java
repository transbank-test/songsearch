package cl.stgo.songsearch.localData.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import cl.stgo.songsearch.models.Song;

@Dao
public interface SongDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertSong(Song song);

    /**
     * SELECT * FROM YOUR_TABLE LIMIT 20 OFFSET 0 //for page #1
     * SELECT * FROM YOUR_TABLE LIMIT 20 OFFSET 20 // for
     * **/

    @Transaction
    @Query("SELECT * FROM song where name LIKE '%' || :name || '%'  OR albumName LIKE '%' || :name || '%' OR artistName LIKE '%' || :name || '%' LIMIT 20 OFFSET :page*20")
    public List<Song> getSongPage(String name, int page);

    // update image
    @Query("UPDATE song SET image=:image WHERE trackId = :id")
    void updateImage(byte[] image, String id);

    // update preview
    @Query("UPDATE song SET preview=:preview WHERE trackId = :id")
    void updatePreview(byte[] preview, String id);


}
