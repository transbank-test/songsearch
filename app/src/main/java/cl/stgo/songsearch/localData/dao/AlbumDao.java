package cl.stgo.songsearch.localData.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import java.util.List;

import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.AlbumWithSong;

@Dao
public interface AlbumDao {


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAlbum(Album album);

    @Transaction
    @Query("SELECT * FROM album WHERE albumId = :id LIMIT 1")
    public Album getAlbum(int id);

    @Transaction
    @Query("SELECT * FROM album WHERE albumId = :id")
    public List<AlbumWithSong> getAlbumSong(int id);

}
