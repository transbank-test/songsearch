package cl.stgo.songsearch.adapters;

import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import cl.stgo.songsearch.R;
import cl.stgo.songsearch.databinding.RowLoadingBinding;
import cl.stgo.songsearch.databinding.RowSongBinding;
import cl.stgo.songsearch.models.Song;

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private static MediaPlayer mediaPlayer;
    Handler durationHandler;
    private ProgressBar progressBar;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private String previosSongId;

    private double timeElapsed = 0, finalTime = 0;


    private List<Song> songList;
    private Context context;
    private Activity activity;
    private songClickListener songClickListener;
    private boolean isAlbum = false;

    public SearchAdapter(List<Song> songList, Context context) {
        this.songList = songList;
        this.context = context;
    }

    public SearchAdapter(List<Song> songList, Activity activity, boolean isAlbum) {
        this.songList = songList;
        this.activity = activity;
        this.context = activity.getBaseContext();
        this.isAlbum = isAlbum;
    }

    public void setSongClickListener(SearchAdapter.songClickListener songClickListener) {
        this.songClickListener = songClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_ITEM) {
            return new ItemViewHolder(RowSongBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        } else {
            return new LoadingViewHolder(RowLoadingBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) viewHolder, position);
        } else if (viewHolder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        }
    }

    @Override
    public int getItemCount() {
        return songList == null ? 0 : songList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return songList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    public class ItemViewHolder extends RecyclerView.ViewHolder {
        private RowSongBinding rowSongBinding;

        public ItemViewHolder(@NonNull RowSongBinding rowSongBinding) {
            super(rowSongBinding.getRoot());
            this.rowSongBinding = rowSongBinding;
        }
    }


    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        private RowLoadingBinding rowLoadingBinding;

        public LoadingViewHolder(@NonNull RowLoadingBinding rowLoadingBinding) {
            super(rowLoadingBinding.getRoot());
            this.rowLoadingBinding = rowLoadingBinding;
        }
    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed
    }

    private void populateItemRows(ItemViewHolder viewHolder, int position) {
        final Song item = songList.get(position);
        viewHolder.rowSongBinding.songItemName.setText(item.getName());
        viewHolder.rowSongBinding.songItemArtist.setText(item.getArtistName());
        viewHolder.rowSongBinding.songItemAlbum.setText(item.getAlbumName());
        viewHolder.rowSongBinding.getRoot().setOnClickListener(v -> {
            if (!isAlbum)
                songClickListener.onSongClick(item.getAlbumId());
        });
        viewHolder.rowSongBinding.songPlayProgress.setMax(100);
        if (isAlbum){
            viewHolder.rowSongBinding.songPlayProgress.setVisibility(View.VISIBLE);
            viewHolder.rowSongBinding.songPlayButton.setOnClickListener(v -> {
                try {
                    if (mediaPlayer == null){
                        timeElapsed = 0;
                        previosSongId = item.getTrackId();
                        playAudio(context, item.getPreviewUrl());
                        viewHolder.rowSongBinding.songPlayButton.setImageDrawable(context.getDrawable(R.drawable.ic_pause));

                        Timer timerAsync = new Timer();
                        TimerTask timerTaskAsync = new TimerTask() {
                            @Override
                            public void run() {
                                activity.runOnUiThread(new Runnable() {
                                    @Override public void run() {
                                        if (mediaPlayer != null){
                                            timeElapsed = mediaPlayer.getCurrentPosition();
                                        }
                                        viewHolder.rowSongBinding.songPlayProgress.setProgress((int)((int)(timeElapsed * 100)/ finalTime), true);
                                    }
                                });
                            }
                        };
                        timerAsync.schedule(timerTaskAsync, 0, 100);

                        //durationHandler.postDelayed(updateSeekBarTime, 100);
                        //durationHandler.postDelayed(updateSeekBarTime, 100);

                    }
                    else {
                        if (!previosSongId.equals(item.getTrackId())){
                            mediaPlayer.pause();
                            killMediaPlayer();
                            viewHolder.rowSongBinding.songPlayButton.setImageDrawable(context.getDrawable(R.drawable.ic_play));
                            viewHolder.rowSongBinding.songPlayProgress.setProgress(0);
                            playAudio(context, item.getPreviewUrl());
                        }else {
                            if (mediaPlayer.isPlaying()) {
                                mediaPlayer.pause();
                                viewHolder.rowSongBinding.songPlayButton.setImageDrawable(context.getDrawable(R.drawable.ic_play));
                            } else {
                                mediaPlayer.start();
                                viewHolder.rowSongBinding.songPlayButton.setImageDrawable(context.getDrawable(R.drawable.ic_pause));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            viewHolder.rowSongBinding.songPlayButton.setVisibility(View.VISIBLE);
            viewHolder.rowSongBinding.songImage.setVisibility(View.GONE);
        }

        Glide.with(context)
                .load(item.getArtworkUrl())
                .centerCrop()
                .into(viewHolder.rowSongBinding.songImage);
    }


    public void playAudio(final Context context, final String url) throws Exception {
        durationHandler = new Handler(Looper.getMainLooper());
        if (mediaPlayer == null) {
            mediaPlayer = MediaPlayer.create(context, Uri.parse(url));
            finalTime = mediaPlayer.getDuration();
        }
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                killMediaPlayer();
            }
        });

        mediaPlayer.start();
    }


    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            if (mediaPlayer != null){
                timeElapsed = mediaPlayer.getCurrentPosition();
            }
        }
    };

    private static void killMediaPlayer() {
        if (mediaPlayer != null) {
            try {
                mediaPlayer.reset();
                mediaPlayer.release();
                mediaPlayer = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface songClickListener{
        void onSongClick(String albumId);
    }
}
