package cl.stgo.songsearch.features.seach;


import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import cl.stgo.songsearch.R;
import cl.stgo.songsearch.api.ApiClient;
import cl.stgo.songsearch.api.ApiInterface;
import cl.stgo.songsearch.api.ResponseDAO;
import cl.stgo.songsearch.localData.AppDatabase;
import cl.stgo.songsearch.models.Song;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragmentInteractor {

    private AppDatabase mDb;
    private Context context;

    public SearchFragmentInteractor(Context context) {
        this.context = context;
    }

    public void search(String songName, boolean localSearch, SearchCallback callback){
        if (localSearch)
            localSearch(songName, 0, callback);
        else
            remoteSearch(songName, callback);
    }

    public void localSearch(String songName, int page, SearchCallback callback){
        getLocalSong(songName, page, callback);
    }

    private void remoteSearch(String songName, SearchCallback callback){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseDAO> call = apiService.searchSongs(songName);
        call.enqueue(new Callback<ResponseDAO>() {
            @Override
            public void onResponse(Call<ResponseDAO> call, Response<ResponseDAO> response) {
                if (response.code() != 200){
                    callback.onSearchError(context.getString(R.string.error_connect_service));
                }else {
                    if (response.body().getResultCount() > 0){
                        saveSongs(response.body().getResults(), callback);
                    }else {
                        callback.onSearchError(context.getString(R.string.error_not_found));
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseDAO> call, Throwable t) {
                callback.onSearchError(context.getString(R.string.error_connect_service));
            }
        });
    }

    private void saveSongs(ArrayList<Song> songs, SearchCallback callback){
        mDb = AppDatabase.getInstance(context);

        Executor myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.execute(() -> {
            for (Song s: songs) {
                try {
                    mDb.songDao().insertSong(s);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            callback.onSearchSuccess();
        });
    }

    private void getLocalSong(String name, int page, SearchCallback callback){
        mDb = AppDatabase.getInstance(context);
        Executor myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.execute(() -> {
            List<Song> songs = mDb.songDao().getSongPage(name, page);
            callback.onLoadSongSuccess(songs);
        });
    }
}
