package cl.stgo.songsearch.features.album;

import java.util.List;

import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.AlbumWithSong;

public class AlbumFragmentPresenter implements AlbumCallback{

    private AlbumFragmentView view;
    private AlbumFragmentInteractor interactor;
    private String lastAlbumId;


    public AlbumFragmentPresenter(AlbumFragmentView view, AlbumFragmentInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void loadAlbum(String albumId, boolean localAlbum){
        this.lastAlbumId = albumId;
        showProgress();
        interactor.getAlbum(albumId, localAlbum, this);
    }

    private void showProgress(){
        if (viewNotNull())
            view.showProgress();
    }

    private void hideProgress(){
        if (viewNotNull())
            view.hideProgress();

    }

    private boolean viewNotNull(){
        if (view != null)
            return true;
        else
            return false;
    }

    @Override
    public void onGetAlbumSuccess() {
        interactor.getLocalSong(lastAlbumId, this);
    }

    @Override
    public void onGetAlbumError(String message) {
        hideProgress();
        view.showAlertDialog(message);
    }

    @Override
    public void onLoadAlbumError(String message) {
        hideProgress();
        view.showAlertDialog(message);
    }

    @Override
    public void onLoadSongSuccess(List<AlbumWithSong> songs) {
        hideProgress();
        view.loadAlbum(songs.get(0).album);
        view.loadAlbumList(songs.get(0).songs);
    }

    @Override
    public void onLoadSongError(String message) {
        hideProgress();
        view.showAlertDialog(message);
    }
}
