package cl.stgo.songsearch.features.album;

import java.util.List;

import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.AlbumWithSong;

public interface AlbumCallback {
    void onGetAlbumSuccess();
    void onGetAlbumError(String message);
    void onLoadAlbumError(String message);
    void onLoadSongSuccess(List<AlbumWithSong> songs);
    void onLoadSongError(String message);
}
