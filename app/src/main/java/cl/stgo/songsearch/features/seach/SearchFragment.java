package cl.stgo.songsearch.features.seach;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cl.stgo.songsearch.activities.MainActivity;
import cl.stgo.songsearch.adapters.SearchAdapter;
import cl.stgo.songsearch.databinding.FragmentSearchBinding;
import cl.stgo.songsearch.models.Song;
import cl.stgo.songsearch.utils.DialogUtil;
import cl.stgo.songsearch.utils.Utils;

public class SearchFragment extends Fragment implements SearchFragmentView{

    private long mLastClickTime = 0;
    private int page = 0;

    private static SearchFragment instance =  null;
    FragmentSearchBinding binding = null;

    SearchAdapter searchAdapter;
    List<Song> songList = new ArrayList<>();
    boolean isLoading = false;

    private int currentSize = 0;
    private int nextLimit = 0;

    private SearchFragmentPresenter presenter;
    private SearchFragment() {

    }

    public static SearchFragment newInstance() {
        if (instance == null)
            instance = new SearchFragment();
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentSearchBinding.inflate(inflater, container, false);
        init();
        return binding.getRoot();
    }

    private void init(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.searchSongList.setLayoutManager(linearLayoutManager);

        initScrollListener();


        presenter = new SearchFragmentPresenter(this, new SearchFragmentInteractor(getContext()));
        binding.searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 3000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                enabledSeach(false);
                showProgress();
                if (!Utils.getInstance(getContext()).isOnlineDevice()){
                    hideProgress();
                    DialogUtil.getInstance(getActivity()).showOffLineAlert(new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            searchSong(true);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
                }else {
                    searchSong(false);
                }
            }
        });
    }

    private void searchSong(boolean localSearch){
        presenter.searchSong(getSeachText(), localSearch);
    }

    private String getSeachText(){
        return binding.searchInput.getEditText().getText().toString().trim();
    }
    @Override
    public void showProgress() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                binding.searchProgressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideProgress() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                binding.searchProgressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void showAlertDialog(String message) {
        DialogUtil.getInstance(getActivity()).showErroAlert(message);
    }

    @Override
    public void loadSearchList(List<Song> songs) {

        if (page > 0){
            addNewSongs(songs);
        }else {
            this.songList = songs;
            initAdapter();
        }
    }

    private void addNewSongs(List<Song> songs){

        Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(() -> {
            songList.remove(songList.size() - 1);
            int scrollPosition = songList.size();
            searchAdapter.notifyItemRemoved(scrollPosition);
            currentSize = scrollPosition;
            nextLimit = currentSize + 20;
            songList.addAll(songs);
            currentSize = songs.size();
            searchAdapter.notifyDataSetChanged();
            isLoading = false;
        }, 2000);
    }

    @Override
    public void enabledSeach(boolean enable) {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                binding.searchInput.getEditText().setEnabled(enable);
                binding.searchButton.setEnabled(enable);
                binding.searchButton.setClickable(enable);            }
        });
    }

    private void initAdapter() {
        searchAdapter = new SearchAdapter(songList, getContext());
        searchAdapter.setSongClickListener(albumId -> ((MainActivity) getActivity()).openBottomSheetDialog(albumId));
        getActivity().runOnUiThread(() -> binding.searchSongList.setAdapter(searchAdapter));
    }


    private void initScrollListener() {
        binding.searchSongList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == songList.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });


    }

    private void loadMore() {
        page += 1;
        songList.add(null);
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                searchAdapter.notifyItemInserted(songList.size() - 1);
            }
        });
        presenter.loadSong(getSeachText(), page);
    }
}