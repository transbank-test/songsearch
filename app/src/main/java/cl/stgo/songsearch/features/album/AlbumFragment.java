package cl.stgo.songsearch.features.album;

import android.os.Bundle;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.LinearLayoutManager;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import java.util.ArrayList;
import java.util.List;

import cl.stgo.songsearch.adapters.SearchAdapter;
import cl.stgo.songsearch.databinding.FragmentAlbumBinding;
import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.Song;
import cl.stgo.songsearch.utils.DialogUtil;
import cl.stgo.songsearch.utils.Utils;

public class AlbumFragment extends BottomSheetDialogFragment implements AlbumFragmentView{

    private static final String ALBUM_ID = "albumId";

    private static AlbumFragment instance = null;
    FragmentAlbumBinding binding = null;

    SearchAdapter searchAdapter;
    List<Song> songList = new ArrayList<>();
    AlbumFragmentPresenter presenter;

    private String albumId;

    private AlbumFragment() {

    }

    public static AlbumFragment newInstance(String albumId) {
        if (instance == null){
            instance = new AlbumFragment();
        }
        Bundle args = new Bundle();
        args.putString(ALBUM_ID, albumId);
        instance.setArguments(args);
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null)
            albumId = getArguments().getString(ALBUM_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAlbumBinding.inflate(inflater, container, false);
        init();
        return  binding.getRoot();
    }

    private void init(){
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        binding.albumSongs.setLayoutManager(linearLayoutManager);
        presenter = new AlbumFragmentPresenter(this, new AlbumFragmentInteractor(getContext()));

        if (!Utils.getInstance(getContext()).isOnlineDevice()){
            loadAlbum(true);
        }else {
            loadAlbum(false);
        }

    }

    private void loadAlbum(boolean localAlbum) {
        presenter.loadAlbum(albumId, localAlbum);
    }

    @Override
    public void showProgress() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                binding.albumProgress.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideProgress() {
        getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                binding.albumProgress.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void showAlertDialog(String message) {
        DialogUtil.getInstance(getActivity()).showErroAlert(message);
    }

    @Override
    public void loadAlbumList(List<Song> songs) {
        songList = songs;
        initAdapter();
    }

    @Override
    public void loadAlbum(Album album) {

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Glide.with(getContext())
                        .load(album.getArtworkUrl())
                        .centerCrop()
                        .into(binding.albumImage);
                binding.albumName.setText(album.getAlbumName());
                binding.albumArtist.setText(album.getArtistName());            }
        });
    }

    @Override
    public void showPlayProgress() {

    }

    @Override
    public void hidePlayProgress() {

    }

    private void initAdapter() {
        searchAdapter = new SearchAdapter(songList, getActivity(), true);
        getActivity().runOnUiThread(() -> binding.albumSongs.setAdapter(searchAdapter));
    }
}