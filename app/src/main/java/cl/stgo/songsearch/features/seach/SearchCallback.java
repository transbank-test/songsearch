package cl.stgo.songsearch.features.seach;

import java.util.List;

import cl.stgo.songsearch.models.Song;

public interface SearchCallback {

    void onSearchSuccess();
    void onSearchError(String message);
    void onLoadSongSuccess(List<Song> songs);
    void onLoadSongError(String message);

}
