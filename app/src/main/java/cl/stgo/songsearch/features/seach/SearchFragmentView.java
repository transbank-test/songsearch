package cl.stgo.songsearch.features.seach;

import android.app.AlertDialog;

import java.util.List;

import cl.stgo.songsearch.adapters.SearchAdapter;
import cl.stgo.songsearch.models.Song;

public interface SearchFragmentView {

    void showProgress();

    void hideProgress();

    void showAlertDialog(String message);

    void loadSearchList(List<Song> songs);

    void enabledSeach(boolean enable);

}
