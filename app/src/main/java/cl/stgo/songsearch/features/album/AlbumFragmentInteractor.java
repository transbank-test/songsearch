package cl.stgo.songsearch.features.album;

import android.content.Context;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import cl.stgo.songsearch.R;
import cl.stgo.songsearch.api.ApiClient;
import cl.stgo.songsearch.api.ApiInterface;
import cl.stgo.songsearch.api.ResponseDAO;
import cl.stgo.songsearch.localData.AppDatabase;
import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.AlbumWithSong;
import cl.stgo.songsearch.models.Song;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlbumFragmentInteractor {

    private AppDatabase mDb;
    private Context context;

    public AlbumFragmentInteractor(Context context) {
        this.context = context;
    }

    public void getAlbum(String albumId, boolean localAlbum, AlbumCallback callback){
        if (localAlbum)
            getLocalSong(albumId, callback);
        else
            getRemoteAlbum(albumId, callback);
    }

    public void getLocalSong(String albumId, AlbumCallback callback){
        loadLocalSong(Integer.parseInt(albumId), callback);
    }

    public void getRemoteAlbum(String albumId, AlbumCallback callback){
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ResponseDAO> call = apiService.searchAlbum(albumId);
        call.enqueue(new Callback<ResponseDAO>() {
            @Override
            public void onResponse(Call<ResponseDAO> call, Response<ResponseDAO> response) {
                if (response.code() != 200){
                    callback.onGetAlbumError(context.getString(R.string.error_connect_service));
                }else {
                    if (response.body().getResultCount() > 0){
                        processResponse(response.body(), callback);
                    }else {
                        callback.onGetAlbumError(context.getString(R.string.error_not_found));
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseDAO> call, Throwable t) {
                callback.onGetAlbumError(context.getString(R.string.error_connect_service));
            }
        });
    }


    private void processResponse(ResponseDAO responseDAO, AlbumCallback callback){
        mDb = AppDatabase.getInstance(context);
        Executor myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.execute(() -> {
            for (Song s: responseDAO.getResults()) {
                if (s.getType().equals("Album")) {
                    Album album = new Album();
                    album.setAlbumId(Integer.parseInt(s.getAlbumId()));
                    album.setAlbumName(s.getAlbumName());
                    album.setArtistName(s.getArtistName());
                    album.setArtworkUrl(s.getArtworkUrl());
                    album.setType(s.getType());
                    mDb.albumDao().insertAlbum(album);
                }
                else
                    mDb.songDao().insertSong(s);
            }
            callback.onGetAlbumSuccess();
        });
    }

    private void loadLocalSong(int albumId, AlbumCallback callback){
        mDb = AppDatabase.getInstance(context);
        Executor myExecutor = Executors.newSingleThreadExecutor();
        myExecutor.execute(() -> {
            List<AlbumWithSong> songs = mDb.albumDao().getAlbumSong(albumId);
            callback.onLoadSongSuccess(songs);
        });
    }

}
