package cl.stgo.songsearch.features.album;

import android.app.AlertDialog;

import java.util.List;

import cl.stgo.songsearch.models.Album;
import cl.stgo.songsearch.models.Song;

public interface AlbumFragmentView {
    void showProgress();

    void hideProgress();

    void showAlertDialog(String message);

    void loadAlbumList(List<Song> songs);

    void loadAlbum(Album album);

    void showPlayProgress();

    void hidePlayProgress();
}
