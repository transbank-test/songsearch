package cl.stgo.songsearch.features.seach;

import java.util.List;

import cl.stgo.songsearch.models.Song;

public class SearchFragmentPresenter implements SearchCallback {

    private SearchFragmentView view;
    private SearchFragmentInteractor interactor;
    private String lastSongName;

    public SearchFragmentPresenter(SearchFragmentView view, SearchFragmentInteractor interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void searchSong(String songName, boolean localSearch){
        this.lastSongName = songName;
        view.enabledSeach(false);
        showProgress();
        interactor.search(songName, localSearch, this);
    }

    public void loadSong(String songName, int page){
        this.lastSongName = songName;
        interactor.localSearch(songName, page, this);
    }

    @Override
    public void onSearchSuccess() {
        interactor.localSearch(lastSongName, 0, this);
    }

    @Override
    public void onSearchError(String message) {
        hideProgress();
        view.showAlertDialog(message);
        view.enabledSeach(true);
    }

    @Override
    public void onLoadSongSuccess(List<Song> songs) {
        hideProgress();
        view.loadSearchList(songs);
        view.enabledSeach(true);
    }

    @Override
    public void onLoadSongError(String message) {
        view.showAlertDialog(message);
    }

    private void showProgress(){
        if (viewNotNull())
            view.showProgress();
    }

    private void hideProgress(){
        if (viewNotNull())
            view.hideProgress();
    }

    private boolean viewNotNull(){
        if (view != null)
            return true;
        else
            return false;
    }
}
