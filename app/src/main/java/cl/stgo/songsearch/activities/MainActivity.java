package cl.stgo.songsearch.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import cl.stgo.songsearch.R;
import cl.stgo.songsearch.features.album.AlbumFragment;
import cl.stgo.songsearch.features.seach.SearchFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }


    private void init(){
        setFragment(SearchFragment.newInstance());
    }

    public void setFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame, fragment);
        transaction.commit();
    }

    public void openBottomSheetDialog(String albumId) {

        AlbumFragment albumFragment =
                AlbumFragment.newInstance(albumId);
        getSupportFragmentManager().beginTransaction().remove(albumFragment);

        albumFragment.show(getSupportFragmentManager(), albumFragment.getTag());
    }
}