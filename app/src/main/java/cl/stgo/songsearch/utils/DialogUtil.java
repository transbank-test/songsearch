package cl.stgo.songsearch.utils;

import android.app.Activity;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import cl.stgo.songsearch.R;

public class DialogUtil {
    private Activity activity;
    private static DialogUtil instance = null;
    AlertDialog.Builder builder;

    private DialogUtil(Activity activity) {
        this.activity = activity;
    }

    public static DialogUtil getInstance(Activity activity){
        if (instance == null)
            instance = new DialogUtil(activity);
        return instance;
    }


    public void showOffLineAlert(DialogInterface.OnClickListener positive, DialogInterface.OnClickListener negative){

        builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.offline_alert_title)
                .setMessage(R.string.offline_alert_message)
                .setPositiveButton(R.string.dialog_positive, positive)
                .setNegativeButton(R.string.dialog_negative, negative);

        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                builder.show();
            }
        });
    }

    public void showErroAlert(String message){

        builder = new AlertDialog.Builder(activity);
        builder.setTitle(R.string.error_alert_title)
                .setMessage(message)
                .setPositiveButton(R.string.dialog_positive, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                builder.create().show();
            }
        });
    }
}
