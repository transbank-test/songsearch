package cl.stgo.songsearch.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class Utils {

    private static Utils instance;
    private Context context;

    private Utils(Context context) {
        this.context = context;
    }

    public static Utils getInstance(Context context){
        if (instance == null)
            instance = new Utils(context);
        return instance;
    }


    public byte[] getByteArrayFromUrl(String urlStr) throws Exception{
        URL url = new URL(urlStr);
        ByteArrayOutputStream output = new ByteArrayOutputStream();

        try (InputStream inputStream = url.openStream()) {
            int n = 0;
            byte [] buffer = new byte[ 1024 ];
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
        }

        return output.toByteArray();
    }

    public Bitmap getBitmapFomByteArray(byte[] data){
        return BitmapFactory.decodeByteArray(data, 0, data.length);
    }

    public File getMediaFileFromByteArray(byte[] data) throws IOException {
        File mediaFile = File.createTempFile("previewFile", "m4a", context.getCacheDir());
        mediaFile.deleteOnExit();
        FileOutputStream fos = new FileOutputStream(mediaFile);
        fos.write(data);
        fos.close();
        return mediaFile;
    }

    public boolean isOnlineDevice(){
        return isNetDisponible() && isOnlineNet();
    }
    private boolean isNetDisponible() {
        ConnectivityManager connectivityManager = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo actNetInfo = connectivityManager.getActiveNetworkInfo();

        return (actNetInfo != null && actNetInfo.isConnected());
    }

    private Boolean isOnlineNet() {
        try {
            Process p = java.lang.Runtime.getRuntime().exec("/system/bin/ping -c 1 www.google.com");

            int val = p.waitFor();
            return (val == 0);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}

