package cl.stgo.songsearch.models;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class AlbumWithSong {

    @Embedded
    public Album album;
    @Relation(
            parentColumn = "albumId",
            entityColumn = "albumId"
    )
    public List<Song> songs;

}
